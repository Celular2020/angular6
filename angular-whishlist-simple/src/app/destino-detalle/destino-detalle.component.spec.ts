import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DestinoDEtalleComponent } from './destino-detalle.component';

describe('DestinoDEtalleComponent', () => {
  let component: DestinoDEtalleComponent;
  let fixture: ComponentFixture<DestinoDEtalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestinoDEtalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DestinoDEtalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
